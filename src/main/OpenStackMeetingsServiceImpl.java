package main;
import java.util.ListIterator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;



public class OpenStackMeetingsServiceImpl implements MeetingsService {

	private String URLsource = "http://eavesdrop.openstack.org/meetings";
	
	public String getMeetings(String project, String year){
		if(project == null && year == null) {
			return "No project or year parameters entered.";
		}
		if(project == null || project.length() < 1){
			return "No project parameter was entered.";
		}
		else if(year == null || year.length() < 4) {
			return "No year parameter was entered.";
		}
		else{
			String ret = parseSource(project, year, this.URLsource);
			return ret;
		}
		
	}
	
	private String parseSource(String project, String year, String source) {
		Document doc;
		int count = 0;
		try {
			source = source + "/" + project;
			doc = Jsoup.connect(source).get();
		}
		catch (Exception exp) {
			exp.printStackTrace();
			return "\nProject with name " + project + " is not a valid project";
		} 
		try {
			source = source + "/" + year;
			doc = Jsoup.connect(source).get();
		}
		catch (Exception exp) {
			exp.printStackTrace();
			return "\nInvalid year " + year + " for Project named " + project;
		} 
		try {	
			Elements links = doc.select("td");
			if(doc != null) {
		    	ListIterator<Element> iter = links.listIterator();
		    	while(iter.hasNext()) {
		    		Element e = (Element) iter.next();
		    		String s = e.text();
		    		if ( s != null && s.contains(project)) {
		    			count++;
		    		}
		    	}	
		    }  
		} catch (Exception exp) {
			exp.printStackTrace();
		} 
		
		return "Number of meeting files: " + ((Integer)count).toString();
	}

}
