package main;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class OpenStackMeetingsController {

	private MeetingsService openStackMeetingsService;


	public OpenStackMeetingsController() {
		
	}
	
	public OpenStackMeetingsController(MeetingsService openStackMeetingsService) {
		this.openStackMeetingsService = openStackMeetingsService;
	}
	
	@ResponseBody
    @RequestMapping(value = "/")
    public String helloWorld()
    {
        return "Hello world!";
    }
	
	@ResponseBody
	@RequestMapping(value = "/openstackmeetings")
	public String getOpenStackMeetings() {
		return "Enter query parameter to search Open Stack Meetings";
	}
	
	@ResponseBody
    @RequestMapping(value = "/openstackmeetings", params = {"project", "year"}, method=RequestMethod.GET)
    public String getParameters(@RequestParam("project") String project, @RequestParam("year") String year)
    {
		String ret = openStackMeetingsService.getMeetings(project.toLowerCase(), year);
		return ret;
    }
	
	/**
	 * This will catch if only project parameter is entered
	 * @param project
	 * @return
	 */
	@ResponseBody
    @RequestMapping(value = "/openstackmeetings", params = {"project"}, method=RequestMethod.GET)
    public String projectParameter(@RequestParam("project") String project)
    {
		String ret = openStackMeetingsService.getMeetings(project.toLowerCase(), "");
		return ret;
    }
	
	
	/**
	 * This will catch if only year parameter is entered
	 * @param project
	 * @return
	 */
	@ResponseBody
    @RequestMapping(value = "/openstackmeetings", params = {"year"}, method=RequestMethod.GET)
    public String yearParameter(@RequestParam("year") String year)
    {
		String ret = openStackMeetingsService.getMeetings("", year);
		return ret;
    }

	public void setOpenStackMeetingsService(MeetingsService openStackMeetingsService) {
		this.openStackMeetingsService = openStackMeetingsService;
	}
	
}
