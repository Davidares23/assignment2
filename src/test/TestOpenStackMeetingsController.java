package test;

import main.MeetingsService;
import main.OpenStackMeetingsServiceImpl;
import main.OpenStackMeetingsController;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class TestOpenStackMeetingsController {
	OpenStackMeetingsController openStackMeetingsController = new OpenStackMeetingsController();
	MeetingsService mockService = null;
	
	@Before
	public void setUp1() {		
		mockService = mock(MeetingsService.class);		
		openStackMeetingsController.setOpenStackMeetingsService(mockService);
	}
	
	@Test
	public void testHelloWorld() {
		String ret = openStackMeetingsController.helloWorld();
		assertEquals("Hello world!", ret);
	}
	
	
	@Test
	public void testGetOpenStackMeetings() {
		String ret = openStackMeetingsController.getOpenStackMeetings();
		assertEquals("Enter query parameter to search Open Stack Meetings", ret);
	}
	
}
