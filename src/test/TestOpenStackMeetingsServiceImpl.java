package test;

import main.MeetingsService;
import main.OpenStackMeetingsServiceImpl;
import main.OpenStackMeetingsController;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestOpenStackMeetingsServiceImpl {
	
	MeetingsService openStackMeetingsService;
	
	@Before
	public void setup() {
		openStackMeetingsService = new OpenStackMeetingsServiceImpl();
	}


	@Test //1. Test when both operands are null
	public void testGetMeetings1() {
		String ret = openStackMeetingsService.getMeetings(null, null);
		assertEquals("No project or year parameters entered.", ret);
	}

	@Test //2. Test when year parameter is not entered
	public void testGetMeetings2() {
		String ret = openStackMeetingsService.getMeetings("_fuel", null);
		assertEquals("No year parameter was entered.", ret); 
	}
	
	@Test //3. Test when project parameter is not entered
	public void testGetMeetings3() {
		String ret = openStackMeetingsService.getMeetings(null, "2015");
		assertEquals("No project parameter was entered.", ret);
	}
	
	@Test //4. Test when year parameter is not valid for project
	public void testGetMeetings4() {
		String ret = openStackMeetingsService.getMeetings("solum", "2015");
		assertEquals("Number of meeting files: 4", ret); 
	}
	
	@Test //5. Test when only one operands is not entered
	public void testGetMeetings5() {
		String ret = openStackMeetingsService.getMeetings("solum", "2015");
		assertEquals("Number of meeting files: 4", ret);
	}
	
}
